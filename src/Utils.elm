module Utils exposing (..)


(>=>) : a -> b -> ( a, b )
(>=>) =
    (,)


(∈) : a -> List a -> Bool
(∈) =
    List.member


(=>) : a -> b -> ( a, b )
(=>) =
    (,)
