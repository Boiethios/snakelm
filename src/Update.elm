module Update exposing (..)

import Time exposing (Time)
import Model exposing (..)
import Fifo
import Utils exposing (..)


type Msg
    = NextTick Time
    | Key Direction
    | NoOp


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    let
        updatedModel =
            case msg of
                Key direction ->
                    updateKey model direction

                NextTick _ ->
                    model
                        |> updateDirection
                        |> moveSnake
                        |> checkCollisions

                NoOp ->
                    model
    in
        updatedModel >=> Cmd.none


getInput : Model -> Input
getInput model =
    case model.state of
        InProgress input ->
            input

        _ ->
            Debug.crash "Model should have been 'InProgress'"


{-| When user presses a direction key
-}
updateKey : Model -> Direction -> Model
updateKey model key =
    case model.state of
        NotStarted ->
            let
                input =
                    { direction = key
                    , keys = Fifo.fromList <| List.singleton key
                    }
            in
                { model | state = InProgress input }

        InProgress input ->
            let
                state =
                    InProgress
                        { input | keys = Fifo.insert key input.keys }
            in
                { model | state = state }

        GameOver ->
            model


updateDirection : Model -> Model
updateDirection model =
    let
        input =
            getInput model

        ( direction, keys ) =
            Fifo.remove input.keys

        areOppositeDirection =
            case ( input.direction, direction ) of
                ( Up, Just Down ) ->
                    True

                ( Down, Just Up ) ->
                    True

                ( Left, Just Right ) ->
                    True

                ( Right, Just Left ) ->
                    True

                _ ->
                    False

        updatedInput =
            case ( areOppositeDirection, direction ) of
                ( False, Just direction ) ->
                    { input | keys = keys, direction = direction }

                _ ->
                    { input | keys = keys }
    in
        { model | state = InProgress updatedInput }


moveSnake : Model -> ( Model, Coordinate )
moveSnake model =
    let
        input =
            getInput model

        ( x, y ) =
            model.snake.head

        newHead =
            case input.direction of
                Up ->
                    if y > 0 then
                        ( x, y - 1 )
                    else
                        ( x, heightOfGrid - 1 )

                Down ->
                    if y < heightOfGrid - 1 then
                        ( x, y + 1 )
                    else
                        ( x, 0 )

                Left ->
                    if x > 0 then
                        ( x - 1, y )
                    else
                        ( widthOfGrid - 1, y )

                Right ->
                    if x < widthOfGrid - 1 then
                        ( x + 1, y )
                    else
                        ( 0, y )
    in
        ( model, newHead )


checkCollisions : ( Model, Coordinate ) -> Model
checkCollisions ( model, newHead ) =
    let
        newBody =
            model.snake.head :: List.take (List.length model.snake.body - 1) model.snake.body
    in
        if newHead ∈ model.poops || newHead ∈ newBody then
            { model | state = GameOver }
        else if newHead == model.food then
            let
                ( food, seed ) =
                    randomFood model
            in
                { model
                    | poops = model.food :: model.poops
                    , food = food
                    , seed = seed
                    , snake = { body = model.snake.head :: model.snake.body, head = newHead }
                }
        else
            { model | snake = { body = newBody, head = newHead } }



-- HELPERS


popLast : List a -> List a
popLast l =
    List.take (List.length l - 1) l
