module Main exposing (..)

import Html exposing (..)
import Keyboard exposing (KeyCode)
import Time exposing (Time)
import Model exposing (..)
import View exposing (..)
import Update exposing (..)
import Utils exposing (..)


main : Program Never Model Msg
main =
    Html.program
        { init = init >=> Cmd.none
        , view = view
        , update = update
        , subscriptions = subscriptions
        }


subscriptions : Model -> Sub Msg
subscriptions model =
    let
        tickSubscription =
            case model.state of
                InProgress _ ->
                    Time.every (200 * Time.millisecond) NextTick

                _ ->
                    Sub.none

        keysSubscription =
            if model.state /= GameOver then
                Keyboard.downs key
            else
                Sub.none
    in
        Sub.batch
            [ keysSubscription
            , tickSubscription
            ]


key : KeyCode -> Msg
key keycode =
    case keycode of
        37 ->
            Key Left

        39 ->
            Key Right

        40 ->
            Key Down

        38 ->
            Key Up

        _ ->
            NoOp
