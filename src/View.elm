module View exposing (view)

import Html exposing (..)
import Html.Attributes exposing (style)
import Svg exposing (..)
import Svg.Attributes exposing (..)
import Update exposing (Msg)
import Model exposing (..)
import Utils exposing (..)


view : Model -> Html Msg
view model =
    div [ Html.Attributes.style [ "height" => "100vh" ] ]
        [ renderScore model
        , renderSvg model
        ]



-- SCORE


renderScore : Model -> Html msg
renderScore model =
    div
        [ height "10%"
        , Html.Attributes.style
            [ "text-align" => "center"
            , "font-size" => "300%"
            ]
        ]
        [ Html.text <| "Score: " ++ toString (List.length model.poops) ]



-- SVG CANVAS


renderSvg : Model -> Html msg
renderSvg model =
    let
        svgViewBox =
            viewBox <| "0 0 " ++ widthGround ++ " " ++ heightGround
    in
        svg
            [ svgViewBox
            , height "90%"
            , Html.Attributes.style
                [ "margin-left" => "auto"
                , "margin-right" => "auto"
                , "display" => "block"
                ]
            ]
        <|
            renderBackground model
                :: renderFood model
                :: renderPoops model
                ++ renderSnake model


renderBackground : Model -> Html msg
renderBackground model =
    rect
        [ x "0"
        , y "0"
        , width widthGround
        , height heightGround
        , fill "#4DCD43"
        ]
        []


renderFood : Model -> Html msg
renderFood model =
    let
        ( x_, y_ ) =
            model.food
    in
        rect
            [ x <| position x_ 2
            , y <| position y_ 2
            , width "6"
            , height "6"
            , rx "2"
            , ry "2"
            , fill "red"
            ]
            []


renderSnake : Model -> List (Html msg)
renderSnake model =
    let
        partialRender =
            renderRectangle "black" ( "8", "8" )
    in
        List.map partialRender (model.snake.head :: model.snake.body)


renderPoops : Model -> List (Html msg)
renderPoops model =
    let
        partialRender =
            renderRectangle "#8B4513" ( "8", "8" )
    in
        List.map partialRender model.poops



-- HELPERS


widthGround : String
widthGround =
    toString <| widthOfGrid * 10


heightGround : String
heightGround =
    toString <| heightOfGrid * 10


position : Int -> Int -> String
position n offset =
    toString <| offset + 10 * n


renderRectangle : String -> ( String, String ) -> Coordinate -> Html msg
renderRectangle color_ ( w, h ) ( x_, y_ ) =
    rect
        [ x <| position x_ 1
        , y <| position y_ 1
        , width w
        , height h
        , rx "2"
        , ry "2"
        , fill color_
        ]
        []
