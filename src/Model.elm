module Model exposing (..)

import Random
import Fifo
import Utils exposing (..)


type alias Coordinate =
    ( Int, Int )


type Direction
    = Up
    | Down
    | Right
    | Left


type alias Input =
    { direction : Direction
    , keys : Fifo.Fifo Direction
    }


type GameState
    = NotStarted
    | InProgress Input
    | GameOver


type alias Snake =
    { body : List Coordinate
    , head : Coordinate
    }


type alias Model =
    { snake : Snake
    , food : Coordinate
    , seed : Random.Seed
    , poops : List Coordinate
    , state : GameState
    }


widthOfGrid : Int
widthOfGrid =
    20


heightOfGrid : Int
heightOfGrid =
    20


init : Model
init =
    let
        center =
            ( widthOfGrid // 2, heightOfGrid // 2 )

        snake =
            { body = [ center, center ]
            , head = center
            }

        model =
            { snake = snake
            , seed = Random.initialSeed 0
            , food = ( 0, 0 )
            , poops = []
            , state = NotStarted
            }

        ( food, seed ) =
            randomFood model
    in
        { model | food = food, seed = seed }



-- RANDOM functions


randomGenerator : Random.Generator ( Int, Int )
randomGenerator =
    Random.pair
        (Random.int 0 (widthOfGrid - 1))
        (Random.int 0 (heightOfGrid - 1))


randomFood : Model -> ( Coordinate, Random.Seed )
randomFood model =
    let
        randomFood_ model seed =
            let
                ( newFood, newSeed ) =
                    Random.step randomGenerator seed

                snake =
                    model.snake.head :: model.snake.body
            in
                if newFood ∈ snake || newFood ∈ model.poops then
                    randomFood_ model newSeed
                else
                    ( newFood, newSeed )
    in
        randomFood_ model model.seed
